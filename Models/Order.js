const mongoose = require('mongoose');

const OrderSchema = mongoose.Schema({
    customeremail : {
        type: String,
    },
    storeName: {
        type: String,
        // unique: true
    },
    productname: {
        type: String,
    },
    qty: {
        type: String,
    },
    ordertotal: {
        type: Number,
    },
    placedate: {
        type: String,

    }
})



module.exports = mongoose.model('Order', OrderSchema);