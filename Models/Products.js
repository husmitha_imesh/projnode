const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    category: {
        type: String,
        required: true,
    },
    quantity: {
        type: String,
        required: true,
    },
    price: {
        type: String,
        required: true,
    },
    storeName:{
        type: String,
        unique : true,
    },
    vendorEmail:{
        type: String,
        
    }
})

module.exports = mongoose.model('Product', ProductSchema);