const express = require('express');
const router = express.Router();
const verify = require('../Config/VerifyTokenUser');

const OrderModel = require('../Models/Order');
const ProductModel = require('../Models/Products');








/**
 * @swagger
 * /order/addorder:
 *   post:
 *      tags:
 *        - Order
 *      description: post request for add products
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Auth
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: product object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for add order
 * definitions:
 *   loginRequest:
 *     properties:
 *       name:
 *         type: string
 *       description:
 *         type: string
 *       categorey:
 *         type: string
 *       quantity:
 *         type: string
 *       price:
 *         type: string
 *       storeName:
 *         type: string
 *       vendorEmail:
 *         type: string
 *
 */
router.post('/addorder', verify, async(req, res) => {



 console.log("")
  const order = await ProductModel.findOne({name: req.body.name, storeName: req.body.storeName}) 
  // const {quantity, storeName} = await ProductModel.findOne({ name: req.body.name, storeName: req.body.storeName });
    
  if (!order) {
    return res.status(400).send('No product available');
  }
 

  const storequantity = parseInt(order.quantity);
  const recievedquan = parseInt(req.body.qty);


  if(storequantity < recievedquan){
    return res.status(400).send("Not enough products");
  }

 

  if(storeName !== req.body.storeName){
    return res.status(400).send("Cannot select from other stores");
  }



    const emailed = req.user.email;
    

    const addorder = new OrderModel({
      customeremail: emailed,
      storeName: storeName,
      productname: req.body.name,
      qty: req.body.qty,
      ordertotal: 5000,
      
      })

      try{

        const saveorder = await addorder.save();

        res.status(200).send("Place Order");

      }catch(err){
        res.json(err);
      }

})








/**
 * @swagger
 * /order/deleteorder:
 *   post:
 *      tags:
 *        - Order
 *      description: post request for add products
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Auth
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: product object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for add order
 * 
 */

router.delete('/deleteorder/:Oid', verify, async(req, res) => {



  try{
  
      const delorder = await OrderModel.deleteOne({ _id: req.params.Oid});
      res.status(200).send("Successfully deleted!");
      
  }catch(err){
      res.json(err);
  }
  
  
      
  })
  





// router.delete('/deleteStore/:name', verify, async(req, res) => {

//   const emailed = req.user.email;

//   console.log(emailed);

//     const {name, vendorEmail} = await StoreModel.findOne({ name: req.body.name });
//   if (!name) {
//     return res.status(400).send('Name not exist');
//   }

//   console.log(nameexist.email);

//   if(vendorEmail !== emailed){
//     res.status(400).send("Email does not matched");
//   }



// try{

//     const delVendor = await StoreModel.deleteOne({ name: req.body.name});
//     res.status(200).send("Successfully deleted!");
    
// }catch(err){
//     res.json(err);
// }


    
// })



module.exports = router;