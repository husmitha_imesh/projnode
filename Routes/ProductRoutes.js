const express = require('express');
const router = express.Router();
const verify = require('../Config/VerifyToken');

const ProductModel = require('../Models/Products');



/**
 * @swagger
 * /product/addproduct:
 *   post:
 *      tags:
 *        - Product
 *      description: post request for add products
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: product object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for add product
 * definitions:
 *   loginRequest:
 *     properties:
 *       name:
 *         type: string
 *       description:
 *         type: string
 *       categorey:
 *         type: string
 *       quantity:
 *         type: string
 *       price:
 *         type: string
 *       storeName:
 *         type: string
 *       vendorEmail:
 *         type: string
 *
 */
router.post('/addproduct', verify, async(req, res) => {


    const emailed = req.user.email;
    

    const addprod = new ProductModel({
        name: req.body.name,
        description: req.body.description,
        category: req.body.category,
        quantity: req.body.quantity,
        price: req.body.price,
        storeName: req.body.storeName,
        vendorEmail: emailed,
      })

      try{

        const savestore = await addprod.save();

        res.status(200).send("Added product");

      }catch(err){
        res.json(err);
      }
})

module.exports = router;
