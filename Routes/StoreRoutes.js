const express = require('express');
const router = express.Router();
const verify = require('../Config/VerifyToken');
const StoreModel = require('../Models/Store');



/**
 * @swagger
 * /store/addstores:
 *   post:
 *      tags:
 *        - Store
 *      description: post request for add stores
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for login
 * definitions:
 *   loginRequest:
 *     properties:
 *       name:
 *         type: string
 *       description:
 *         type: string
 *       categorey:
 *         type: string
 *       vendorEmail:
 *         type: string
 *
 */
router.post('/addstores', verify, async(req, res) => {


    const emailed = req.user.email;

    const addstore = new StoreModel({
        name: req.body.name,
        description: req.body.description,
        categorey: req.body.categorey,
        vendorEmail: emailed,
      })

      try{

        const savestore = await addstore.save();

        res.status(200).send("Added store");

      }catch(err){
        res.json(err);
      }
})





// router.delete('/deleteStore/:name', verify, async(req, res) => {

//   const emailed = req.user.email;

//   console.log(emailed);

//     const {name, vendorEmail} = await StoreModel.findOne({ name: req.body.name });
//   if (!name) {
//     return res.status(400).send('Name not exist');
//   }

//   console.log(nameexist.email);

//   if(vendorEmail !== emailed){
//     res.status(400).send("Email does not matched");
//   }



// try{

//     const delVendor = await StoreModel.deleteOne({ name: req.body.name});
//     res.status(200).send("Successfully deleted!");
    
// }catch(err){
//     res.json(err);
// }


    
// })









// router.put('/editStore/:name', verify, async (req, res) => {


//   const emailed = req.user.email;
  
//   const {name, vendorEmail} = await StoreModel.findOne({ name: req.body.name });
//   if (!name) {
//     return res.status(400).send('Name not exist');
//   }

  
//   if(vendorEmail !== emailed){
//     res.status(400).send("Email does not matched");
//   }



//     try{
//         const updateVendor = await StoreModel.updateOne({name: req.body.name}, 
//             {$set: {description: req.body.description}});
            
//             res.status(200).send("updated Vendor");
//     }catch(err){
//         res.json(err);
//     }
// });



module.exports = router;