const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const verify = require('../Config/VerifyTokenUser');

const UserModel = require('../Models/User');





/**
 * @swagger
 * /user/adduser:
 *  post:
 *    tags:
 *        - User
 *    description: post request for add user
 *    parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/adminRequest'
 *    responses:
 *      200:
 *        description: Access token returns for adduser
 * definitions:
 *   adminRequest:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       address:
 *         type: string
 */
router.post('/adduser', async(req, res) => {
    const emailexist = await UserModel.findOne({ email: req.body.email });
  if (emailexist) {
    return res.status(400).send('Email already exist');
  }


  let Secrect_message = req.body.password;
  let key = '12345678123456781234567812345678';

  let ciper = crypto.createCipher('aes-256-cbc', key);
  let encrypted = ciper.update(Secrect_message, 'utf8', 'hex');
  encrypted += ciper.final('hex');



  const adduser = new UserModel({
    username: req.body.username,
    email: req.body.email,
    password: encrypted,
    address: req.body.address,
  })



  try{

        const saveUser = await adduser.save();

        const token2 = jwt.sign({ _id: adduser._id, email: adduser.email }, 'abcdef');

        res.status(200).send(token2)

  }catch(err){

    res.json(err);
  }

})




/**
 * @swagger
 * /user/deleteuser:
 *   post:
 *      tags:
 *        - User
 *      description: post request for add vendors
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Auth
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *      responses:
 *          200:
 *           description: A successful token return for add vendor
 *
 */
router.delete('/deleteuser/:email', verify, async(req, res) => {



  try{
  
      const delVendor = await VenderModel.deleteOne({ email: req.user.email});
      res.status(200).send("Successfully deleted!");
      
  }catch(err){
      res.json(err);
  }
  
  
      
  })
  



  /**
 * @swagger
 * /user/edituser:
 *  post:
 *    tags:
 *        - User
 *    description: post request for add user
 *    parameters:
 *       - name: Auth
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/adminRequest'
 *    responses:
 *      200:
 *        description: Access token returns for adduser
 * definitions:
 *   adminRequest:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       address:
 *         type: string
 */
  router.put('/edituser/:email', verify, async (req, res) => {
    try{
        const updateVendor = await VenderModel.updateOne({email: req.user.email}, 
            {$set: {username: req.body.username}});
            
            res.status(200).send("updated Vendor");
    }catch(err){
        res.json(err);
    }
});








/**
 * @swagger
 * /user/login:
 *   post:
 *      tags:
 *        - User
 *      description: post request for login users
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for login
 * definitions:
 *   loginRequest:
 *     properties:
 *          email:
 *            type: string
 *          password:
 *            type: string
 *
 */
router.post('/login', async(req, res) => {
  
  const user = await UserModel.findOne({ email: req.body.email });
  if (!user) {
    return res.status(400).send('Email not found');
  }


  const storedPassword = user.password;
  
  let key = '12345678123456781234567812345678';
  

  
  let dicipher = crypto.createDecipher('aes-256-cbc', key);
  let decrypted = dicipher.update(storedPassword, 'hex', 'utf8');
  decrypted += dicipher.final('utf8');

  
  if (decrypted !== req.body.password) {
    return res.status(400).send('invalid paswword');
  }

  try{

    const tkn = jwt.sign({ _id: user._id, email: user.email }, 'abcdef');

    res.status(200).send(tkn);  

    // res.status(200).send("loggin")


  }catch(err){
    res.json(err);
  }

})

module.exports = router;