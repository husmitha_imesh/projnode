const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const verify = require('../Config/VerifyToken');

const VenderModel = require('../Models/Vendor');


/**
 * @swagger
 * /vendor/addvendor:
 *   post:
 *      tags:
 *        - Vendor
 *      description: post request for add vendors
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for add vendor
 * definitions:
 *   loginRequest:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       address:
 *         type: string
 *
 */
router.post('/addvendor', async(req, res) => {
    const emailexist = await VenderModel.findOne({ email: req.body.email });
  if (emailexist) {
    return res.status(400).send('Email already exist');
  }



  let Secrect_message = req.body.password;
  let key = '12345678123456781234567812345678';

  let ciper = crypto.createCipher('aes-256-cbc', key);
  let encryptec = ciper.update(Secrect_message, 'utf8', 'hex');
  encryptec += ciper.final('hex');



  const addvendor = new VenderModel({
    username: req.body.username,
    email: req.body.email,
    password: encryptec,
    address: req.body.address,
  })



  try{

        const saveVendor = await addvendor.save();

        const token1 = jwt.sign({ _id: addvendor._id, email: addvendor.email }, 'abcd');
    res.status(200).send(token1);

        // res.status(200).send("Vendor added")
  }catch(err){

    res.json(err);
  }

})





/**
 * @swagger
 * /vendor/deleteVendor:
 *   post:
 *      tags:
 *        - Vendor
 *      description: post request for add vendors
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *      responses:
 *          200:
 *           description: A successful token return for add vendor
 *
 */
router.delete('/deleteVendor/:email', verify, async(req, res) => {



try{

    const delVendor = await VenderModel.deleteOne({ email: req.user.email});
    res.status(200).send("Successfully deleted!");
    
}catch(err){
    res.json(err);
}


    
})




/**
 * @swagger
 * /vendor/editVendor:
 *   post:
 *      tags:
 *        - Vendor
 *      description: post request for add vendors
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Authentication
 *         description: 
 *         in: header
 *         required: true
 *         type: string
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for add vendor
 * definitions:
 *   loginRequest:
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *       address:
 *         type: string
 *
 */
router.put('/editVendor/:email', verify, async (req, res) => {
    try{
        const updateVendor = await VenderModel.updateOne({email: req.user.email}, 
            {$set: {username: req.body.username}});
            
            res.status(200).send("updated Vendor");
    }catch(err){
        res.json(err);
    }
});






/**
 * @swagger
 * /vendor/login:
 *   post:
 *      tags:
 *        - Vendor
 *      description: post request for login vendors
 *      produces:
*        - application/json
 *      parameters:
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/loginRequest'
 *      responses:
 *          200:
 *           description: A successful token return for login
 * definitions:
 *   loginRequest:
 *     properties:
 *          email:
 *            type: string
 *          password:
 *            type: string
 *
 */
router.post('/login', async(req, res) => {

  const user = await VenderModel.findOne({ email: req.body.email });
  if (!user) {
    return res.status(400).send('Email not found');
  }

  const storedPassword = user.password;
  
  let key = '12345678123456781234567812345678';
  

  let dicipher = crypto.createDecipher('aes-256-cbc', key);
  let decrypted = dicipher.update(storedPassword, 'hex', 'utf8');
  decrypted += dicipher.final('utf8');


  if (decrypted !== req.body.password) {
    return res.status(400).send('invalid paswword');
  }

  try{

    const tok = jwt.sign({ _id: user._id, email: user.email }, 'abcd');
    res.status(200).send(tok);

    // res.status(200).send("Succesfully login");

  }catch(err){
    res.json(err);
  }



})




module.exports = router;