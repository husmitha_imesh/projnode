const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const swaggerJsDocs = require('swagger-jsdoc');



app.use(bodyparser.json());


const vendorRoute = require('./Routes/VendorRoute');
const UserRoute = require('./Routes/UserRoutes');
const StoreRoute = require('./Routes/StoreRoutes');
const ProductRoute = require('./Routes/ProductRoutes');
const OrderRoute = require('./Routes/OrderRoute');

app.use('/user', UserRoute);
app.use('/vendor', vendorRoute);
app.use('/store', StoreRoute);
app.use('/product', ProductRoute);
app.use('/order', OrderRoute);


const option = {
    swaggerDefinition: {
      info: {
        title: 'Server',
        description: 'Server Side API ',
        contact: {
          name: 'Husmitha'
        },
        server: ['http://localhost:3000']
      }
    },
    apis: ['./Routes/*.js']
  };


  const swaggerDocs = swaggerJsDocs(option);
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
  


mongoose.connect('mongodb://localhost:27017/OrderApp', { useNewUrlParser: true, useUnifiedTopology: true }).then(() => {
    console.log("Db connected");
})







app.listen(3000);